
<img src='https://gitlab.com/gagan0123/wp-plugin-release-example/raw/master/assets/icon-128x128.png' align='right' />

# WP Plugin Release Example #
**Contributors:** [gagan0123](https://profiles.wordpress.org/gagan0123)  
**Donate Link:** PayPal.me/gagan0123  
**Tags:** example, plugin  
**Requires at least:** 4.5  
**Requires PHP:** 5.6  
**Tested up to:** 5.1  
**Stable tag:** 1.1  
**License:** GPLv2  
**License URI:** https://www.gnu.org/licenses/gpl-2.0.html  

This is just a test plugin, that does nothing

## Description ##

This is just a test plugin, that does nothing

## Screenshots ##
### 1. Just WP Dashboard ###
![Just WP Dashboard](https://gitlab.com/gagan0123/wp-plugin-release-example/raw/master/assets/screenshot-1.png)

### 2. Only plugins page in WP Dashboard ###
![Only plugins page in WP Dashboard](https://gitlab.com/gagan0123/wp-plugin-release-example/raw/master/assets/screenshot-2.png)


## Installation ##
1. Don't install

## Changelog ##

### 1.0 ###
* Initial Public Release