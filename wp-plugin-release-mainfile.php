<?php
/**
 * Plugin Name: WP Plugin Release Example
 * Plugin URI:  https://gagan0123.com/
 * Description: Just a sample plugin to demonstrate automated release
 * Version:     1.1
 * Author:      Gagan Deep Singh
 * Author URI:  https://gagan0123.com
 * License:     GPLv2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Domain Path: /languages
 *
 * @package WP_Plugin_Release_Example
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}
