=== WP Plugin Release Example ===
Contributors: gagan0123
Donate Link: PayPal.me/gagan0123
Tags: example, plugin
Requires at least: 4.5
Requires PHP: 5.6
Tested up to: 5.1
Stable tag: 1.1
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This is just a test plugin, that does nothing

== Description ==

This is just a test plugin, that does nothing

== Screenshots ==
1. Just WP Dashboard
2. Only plugins page in WP Dashboard

== Installation ==
1. Don't install

== Changelog ==

= 1.0 =
* Initial Public Release